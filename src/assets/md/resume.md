# Jacob A. Schroeder

j.alexander.sch@gmail.com

937-684-0327

New Carlisle, OH 45344

---

## Summary

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Adept at checking on field personnel and solving concerns with self-directed and flexible approach. Great team management and complex problem-solving skills. Resourceful multitasker with proven skills in scheduling drivers, optimizing routes, and prioritizing calls.

---

## Skills

Proficient:

- Data Entry
- AS400
- Problem Solving
- Computer Skills
- Time Management

Novice:

- Python, Rust, Go, Java, HTML, CSS, and Javascript
- Linux (Primarily used Arch)

---

## Experience

_Dispatch Clerk_

**Estes Express Lines** | 07/2018 to Current

Huber Heights, OH

- Routed and dispatched drivers to companies for freight delivery or pickup.
- Dispatched drivers over the road to other Estes Terminals.
- Input client information into spreadsheets and company database to provide leaders with quick access to essential client data.
- Answered incoming phone calls, assisted customers and other terminals, and directed callers to appropriate departments and personnel.
- Filed documents according to alphanumeric system to promote ease of use and optimal team productivity.
- Trained new team members on multiple aspects of their job.

_Warehouse Worker_

**Meijer** | 05/2015 to 08/2017

Tipp City, OH

- Transferred materials to and from warehouses, production lines, and packaging lines.
- Maneuvered forklift over and around loaded pallets, boxes and other materials.
- Communicated with team members to coordinate efficient and accurate movements.
- Complied with OSHA regulations, company policies and protocols to maintain safe and efficient operations.
- Maintained current forklift training and certification as required by company policies.

_Package Handler_

**FedEx Ground** | 05/2014 to 08/2014

Vandalia, OH

- Sorted cargo for accurate shipment to target locations, preventing unnecessary delays and promoting productivity.
- Tracked parcel movement using hand-held scanners and daily production sheets to keep records accurate.
- Assisted in loading delivery trucks with prepared packages.

_Restaurant Manager_

**C's Pizza** | 01/2012 to 04/2014

Huber Heights, OH

- Trained workers in food preparation, money handling and cleaning roles to facilitate restaurant operations.
- Informed customers of delayed food items and offered estimated times to fill orders.
- Priced and ordered food products, kitchen equipment and food service supplies.
- Maintained operations in full compliance with alcohol service standards and legal requirements to prevent incidents of overserving or underage drinking.
- Reconciled daily transactions, balanced cash registers and deposited restaurant's earnings at bank.
- Assisted restaurant owners with pricing by providing ingredient costs and portioning information.

---

## Education

**Colorado Technical University**

Online

_Computer Science_

**Wright State**

Dayton, OH

_Computer Science_

**Ohio University**

Athens, OH

_Information and Telecommunication Systems_

**Sinclair Community College**

Dayton, OH

_Liberal Arts_

---

## Additional Information

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Co-founded the group 'The Core-Dev Team'. As a group created the program 'RecBoot'. RecBoot was created after one of our team members' iPhone was stuck in a 'Recovery Mode' boot loop. This program aimed to solve this by allowing the phone to boot normally outside of Recovery Mode. The program surpassed 1,000,000+ downloads and was featured in an article in MacWorld magazine.
